#
# Makefile for McastChat(Solaris)
# Kristin Kaeding
# the year 2000.
#
# gcc -g -o McastChat McastChat.c multicast.c -lsocket -lnsl;
#

CXX=gcc
RM= \rm -rf
GZIP=gzip
TAR=tar cvf

INCS= -I
OPTFLAGS=  -g -O4
LIBS= -lthread -lnsl -lsocket -lucb
OBJECTS = multicast.o
COMMONOBJECTS = errexit.c

DISTRIB = McastChat.c multicast.c errexit.c 

CFLAGS = -W -pedantic -ansi $(INCS) $(OPTFLAGS)

.SUFFIXES: .c

all: McastChat

.c.o:
	$(CXX) $(CFLAGS) -c $<

McastChat: McastChat.o $(COMMONOBJECTS) $(OBJECTS)
	$(CXX) $(CFLAGS) McastChat.o $(COMMONOBJECTS) $(OBJECTS) \
	$(LIBS) -o McastChat 


clean:
	rm *.o core McastChat






















