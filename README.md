McastChat
program by Kristin Kaeding, contact@scutigera.com

IP must be multicast
224.0.0.0 THRU 239.255.255.255

Ctrl C to quit
Enter BYE to logoff

FILES NEEDED TO RUN APP: McastChat.c, multicast.c, multicast.h.
All these can be compiled by using the Makefile.

```
% make McastChat
```

this version is written for unix/solaris and will not compile on other platforms-- feel free to play with source code.

My program uses IP multicasting to join a group defined in the command line argument as the following:
```
McastChat <login_name>  <IP_addr>  <Port>
```

After the join is completed successfully, the main thread creates two threads:
TalkThread (Tthread)
ListenThread (Lthread)
and then Mthread waits for both threads to exits using a join statement.

Tthread reads the user inputs from STDIN and sends them as multicast messages to the group as the following: usrName@machine_name: <message>.

Lthread reads multicast messages received from the group and writes them to the STDOUT.

Both threads continue to work, sending and receiving from the group, until user issues BYE string as input to STDIN.

Both threads are terminated (or exit) and the main thread exits.

The program runs as follows: ```McastChat <login_name>  <IP_addr>  <Port>```