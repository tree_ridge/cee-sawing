#ifndef MULTICAST_H
#define MULTICAST_H

/* include files */
#include <stdio.h>
#include <string.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <sys/time.h>
#include <sys/uio.h>
#include <netinet/in.h>
#include <netdb.h>
#include <pwd.h>
#include <arpa/inet.h>

/* define statements */
#define MULTICAST_ADDRESS_BASE  "225.0.0."
#define MULTICAST_PORT_BASE  30000
#define DEFAULT_MULTICAST_TTL_VALUE  32 /* to send datagrams to any host anywhere */

#ifndef	INADDR_NONE
#define	INADDR_NONE	0xffffffff
#endif	/* INADDR_NONE */

#define NAME_LEN 100
#define MAX_LEN 1024

#define TRUE 1
#define FALSE 0

/* functions to support the essential multicasting functions */
extern void leaveGroup(int recvSock, char *group);
extern void joinGroup(int s, char *group);
extern void reusePort(int sock);
extern void setTTLvalue(int s,u_char *i);
extern void setLoopback(int s,u_char loop);

/* external variables */

extern int	errno;
extern char	*sys_errlist[];


#endif /* MULTICAST_H */
