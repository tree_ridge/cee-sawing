/* 
   This file contains code of the functions to add multicasting
   features to a process
*/

#include "multicast.h"
        
/*
   This function removes the process from the group 
*/

void leaveGroup(int recvSock,char *group)
{
  struct ip_mreq dreq;  /* multicast group info structure */

  dreq.imr_multiaddr.s_addr = inet_addr(group);
  dreq.imr_interface.s_addr = INADDR_ANY;  
  if( setsockopt(recvSock,IPPROTO_IP,IP_DROP_MEMBERSHIP,
                (char *) &dreq,sizeof(dreq)) == -1 )
  {
    printf("error in leaving group \n");
    exit(-1);
  }
  
  printf("process quitting multicast group %s \n",group);
}


/*
   This function sets a socket option that allows multipule processes
              to bind to the same port
*/

void reusePort(int s)
{
  int one=1;
  
  if ( setsockopt(s,SOL_SOCKET,SO_REUSEADDR,(char *) &one,sizeof(one)) == -1 )
  {
    printf("error in setsockopt,SO_REUSEPORT \n");
    exit(-1);
  }
}


/*
  This function sets the Time-To-Live value
*/

void setTTLvalue(int s,u_char *ttl_value)
{
  if( setsockopt(s,IPPROTO_IP,IP_MULTICAST_TTL,(char *) ttl_value,
	         sizeof(u_char)) == -1 )
  {
    printf("error in setting loopback value\n");
  }
}


/*
		       
   By default, messages sent to the multicast group are looped 
   back to the local host. this function disables that.
	   loop = 1 /* means enable loopback 
	   loop = 0 /* means disable loopback 
   NOTE : by default, loopback is enabled
*/
       
void setLoopback(int s,u_char loop)
{
  if( setsockopt(s,IPPROTO_IP,IP_MULTICAST_LOOP,(char *) &loop,
                 sizeof(u_char)) == -1 )
  {
    printf("error in disabling loopback\n");
  }
}


/* 
   This function sets the socket option to make the local host 
   join the mulicast group 
*/

void joinGroup(int s, char *group)
{
  struct ip_mreq mreq;  /* multicast group info structure */

  mreq.imr_multiaddr.s_addr = inet_addr(group);
  mreq.imr_interface.s_addr = INADDR_ANY;  
  if ( setsockopt(s,IPPROTO_IP,IP_ADD_MEMBERSHIP,(char *) &mreq,
		  sizeof(mreq)) == -1 )
  {
    printf("error in joining group \n");
    exit(-1);
  }
}








